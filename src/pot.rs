use crate::network::Network;
use serde::{Deserialize, Serialize};

/// __Process Organization Template__: A template used to organize a
/// set of processes.
#[derive(Default, Deserialize, Serialize)]
pub struct Pot {
    /// Unique name of the pot
    pub name: String,
    /// Base distro
    pub rootfs: String,
    /// Command to run when pot is opened
    pub ingredients: Vec<String>,
    /// Type of network to use for the pot
    pub network: Network,
}

impl Pot {
    /// Add ingredient into pot, if the pot is not already cooking
    /// start cooking it.
    pub fn sprinkle(&self, ingredient_index: usize) {
        if self.is_cooking() {
            self.stir("");
        } else {
            self.cook("")
        }
    }

    /// Check if pot is cooking
    pub fn is_cooking(&self) -> bool {
        unimplemented!()
    }
    /// Open a pot that is not cooking and cook it
    ///
    /// 1. First we need to create a new process in a new namespace
    /// using the `unshare` system call. This new namespace will be
    /// the namespace for all processes running in the pot.
    /// 2. link the pot's root process `/proc/<PID>/ns` directory to
    /// the `~/.pot/pots/<POT>/cooking`. This will allow new processes
    /// to use these as references when they wish to
    /// join the namespace (enter the pot)
    fn cook(&self, cmd: &str) {
        unimplemented!()
    }

    /// Open a cooking pot
    fn stir(&self, cmd: &str) {
        unimplemented!()
    }
}
